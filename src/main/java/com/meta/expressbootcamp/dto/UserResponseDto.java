package com.meta.expressbootcamp.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserResponseDto {

    private String contactNumber;
    private String fullName;
    private String email;
}
