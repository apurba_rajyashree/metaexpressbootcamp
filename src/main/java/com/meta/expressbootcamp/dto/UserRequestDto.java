package com.meta.expressbootcamp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserRequestDto {

    private String contactNumber;
    private String fullName;
    private String email;
    private String password;
    private String identificationNumber;
}
