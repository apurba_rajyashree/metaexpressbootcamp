package com.meta.expressbootcamp.enums;

import lombok.Getter;

@Getter
public enum Months {

   JAn,FEB,MAR;

}
