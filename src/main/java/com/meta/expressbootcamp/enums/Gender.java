package com.meta.expressbootcamp.enums;

import lombok.Getter;

@Getter
public enum Gender {
    MALE("MALE", "Male", "पुरुष"),
    FEMALE("FEMALE", "Female", "महिला");

    private String valueEnglish;

    Gender(String valueEnglish, String valueNepali, String key) {
        this.valueEnglish = valueEnglish;
        this.valueNepali = valueNepali;
        this.key = key;
    }

    private String valueNepali;
    private String key;
}
