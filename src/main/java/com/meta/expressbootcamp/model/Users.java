package com.meta.expressbootcamp.model;

import com.meta.expressbootcamp.enums.Gender;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "users", uniqueConstraints = {@UniqueConstraint(name = "uk_users_email", columnNames = "email"),
        @UniqueConstraint(name = "uk_users_contactnumber", columnNames = "contact_number")})
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Users extends BaseEntityClass {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_seq_gen")
    @SequenceGenerator(name = "user_id_seq_gen", sequenceName = "user_id_seq", allocationSize = 1)
    private Integer id;

    @Column(name = "full_name", nullable = false, length = 30)
    private String fullName;

    @Column(name = "email", nullable = false, length = 200)
    private String email;

    @Column(name = "contact_number", nullable = false, length = 13)
    private String contactNumber;

    @Column(name = "age")
    private Integer age;

    @Column(name = "weight")
    private Float weight;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Column(name = "user_name_nepali", nullable = false, length = 50)
    private String userNameNepoli;

    @Transient
    private Integer classBoot;

    @OneToOne(fetch = FetchType.LAZY) //by default eager fetch type
    @JoinColumn(name = "identity_document_id", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_users_identitydocument"))
    private IdentityDocument identityDocument;


    @OneToMany(mappedBy = "users")  //by default lazy fetch type
    private List<Address> addressList;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "users_role",
            joinColumns = {@JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "fk_userrole_user"))},
            inverseJoinColumns = {@JoinColumn(name = "role_id", foreignKey = @ForeignKey(name = "fk_userrole_role"))}
    )
    private List<Role> roles;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender", length = 10)
    private Gender gender;

}

