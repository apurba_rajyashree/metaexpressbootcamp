package com.meta.expressbootcamp.model;

import com.meta.expressbootcamp.enums.Months;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "role")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id; //1

    @Column(name = "role_name")
    private String roleName;

    @Column(name = "role_details")
    private String roleDetails;

    @ElementCollection
    private List<Months> monthsList;


}
