package com.meta.expressbootcamp.repo;

import com.meta.expressbootcamp.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AddressRepo extends JpaRepository<Address, Integer> {

    Optional<Address> findById(Integer id);

    @Override
    List<Address> findAll();

    Optional<Address> findByAge(Integer age);
}
