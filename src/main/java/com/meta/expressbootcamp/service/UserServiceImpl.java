package com.meta.expressbootcamp.service;

import com.meta.expressbootcamp.dto.UserRequestDto;
import com.meta.expressbootcamp.dto.UserResponseDto;
import com.meta.expressbootcamp.enums.Gender;
import com.meta.expressbootcamp.exceptions.DataNotFoundException;
import com.meta.expressbootcamp.model.Address;
import com.meta.expressbootcamp.model.Users;
import com.meta.expressbootcamp.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepo userRepo;

    public UserServiceImpl(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public void create(UserRequestDto userDto) throws RuntimeException {
        String userGenger = Gender.MALE.getKey();

        UserResponseDto userResponseDto = new UserResponseDto();
        userResponseDto.setEmail(userDto.getEmail());
        userResponseDto.setContactNumber(userDto.getContactNumber());
        userResponseDto.setFullName(userDto.getFullName());
        Optional<Users> optionalUser = userRepo.findById(6);
        List<Users> usersList = userRepo.findAll();


        if (optionalUser.isPresent()) {
            Users users = optionalUser.get();
        } else if (optionalUser.isEmpty()) {
            throw new DataNotFoundException("No users");
        }

        try{
            UserResponseDto userResponseDto1 = UserResponseDto.builder()
                    .email(userDto.getEmail())
                    .contactNumber(userDto.getContactNumber())
                    .fullName(userDto.getFullName())
                    .build();
        }catch (Exception e){
            throw new RuntimeException("Problem creating user responnse dto object");
        }finally {
        }


        Users users= Users.builder()
                .id(6)
                .fullName("Apurba")
                .age(22)
                .gender(Gender.FEMALE)
                .build();

        List<Users> usersList1=new ArrayList<>();
        userRepo.save(users);
        userRepo.delete(users);
        userRepo.saveAll(usersList1);
        userRepo.findById(6);



    }
}
