package com.meta.expressbootcamp.service;

import com.meta.expressbootcamp.dto.UserRequestDto;

public interface UserService {
    void create (UserRequestDto userDto);
}
