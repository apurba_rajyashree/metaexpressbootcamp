package com.meta.expressbootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExpressBootCampApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExpressBootCampApplication.class, args);
    }

}
